extends Node2D

onready var bird = get_node("Bird")
onready var animation = get_node("backgroundAnim")
onready var timeToReplay = get_node("TimeToReplay")
onready var pointsLabel = get_node("ControlsNode/Controls/PointsLabel")
onready var birdAnim = get_node("Bird/BirdAnim")

# Sounds
onready var hit_sound = get_node("hit_sound")
onready var point_sound = get_node("point_sound")

# User Points
var points = 0

var state = 1

const PLAYING = 1
const LOSING = 2

func _ready():
	pass

func kill():
	hit_sound.play()
	bird.apply_impulse(Vector2(0, 0), Vector2(-1000, 0))
	animation.stop()
	state = LOSING
	birdAnim.stop()
	# birdAnim.set_animation("bird_hit")
	birdAnim.play("bird_hit")
	timeToReplay.start()

func addPoint():
	points += 1
	point_sound.play()
	pointsLabel.set_text(str(points))

func _on_TimeToReplay_timeout():
	get_tree().reload_current_scene()
