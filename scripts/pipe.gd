extends Node2D

# The pipe Velocity
export var velocity = -200
# The game Scene
onready var scene = get_tree().get_current_scene()

func _ready():
	set_process(true)
	
func _process(delta):
	if scene.state == scene.PLAYING:
		set_pos(get_pos() + Vector2(velocity * delta, 0))
	
	if get_pos().x < -100:
		queue_free()

func _on_Area2D_body_enter( body ):
	if body.get_name() == "Bird":
		scene.kill()

func _on_PointArea_body_enter( body ):
	if body.get_name() == "Bird":
		scene.addPoint()
		print("Current points: ", scene.points);
