extends Position2D

# We get the pipe
onready var pipe = preload("res://scenes/pipe.tscn")

func _ready():
	randomize()


func _on_Timer_timeout():
	var newPipe = pipe.instance();
	newPipe.set_pos(get_pos() + Vector2(0, rand_range(-500, 500)))
	print("New Pipe created at: ", newPipe.get_pos())
	get_owner().add_child(newPipe)
	newPipe.velocity = -400
