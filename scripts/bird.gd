extends RigidBody2D

# sounds
onready var flap_sound = get_node("flap_sound")
onready var scene = get_tree().get_current_scene()

func _ready():
	set_process_input(true)

func _input(event):
	if event.is_action_pressed("touch"):
		if scene.state != scene.LOSING:
			on_touch()

func on_touch():
	if scene.state == scene.PLAYING:
		apply_impulse(Vector2(0,0), Vector2(0, -1000))
		flap_sound.play()
